# Application Programming Interface (API)

Create Apis using python

## Getting started

APIs are used as a means of information interchange between two computers. It facilitates CI/CD and other updated fast and secure

Technical knowhow on how to create APIs using python.

We will create and deploy python based APIs to AWS api gateway

## Terminologies

- [ ] [HTTP](https://www.pcmag.com/encyclopedia/term/http)Hypertext transfer protocol - GET/POST
- [ ] [URL](https://en.wikipedia.org/wiki/URL) Uniform resource locator 
- [ ] [JSON](https://en.wikipedia.org/wiki/JSON) Javascript object notation - Most commonly used data structure. Second is XML 
- [ ] [REST](https://www.redhat.com/en/topics/api/what-is-a-rest-api) Representational state transfer 


## REST

- Stateless
- Client-Server
- Uniform interface
- cacheable
- Layered System
- Code on Demand







command:

```
cd existing_repo
git remote add origin https://gitlab.com/web-apis/python/createapis.git
git branch -M main
git push -uf origin main
```
